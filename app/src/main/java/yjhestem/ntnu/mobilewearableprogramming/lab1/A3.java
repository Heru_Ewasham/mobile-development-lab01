package yjhestem.ntnu.mobilewearableprogramming.lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class A3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);
    }

    public void saveName(View view) {
        Intent returnIntent = new Intent();
        EditText editText = (EditText) findViewById(R.id.T4);
        String name = editText.getText().toString();
        returnIntent.putExtra("result",name);
        setResult(A3.RESULT_OK,returnIntent);
        finish();
    }

    public void cancel(View view) {
        Intent returnIntent = new Intent();
        setResult(A3.RESULT_CANCELED, returnIntent);
        finish();
    }
}
