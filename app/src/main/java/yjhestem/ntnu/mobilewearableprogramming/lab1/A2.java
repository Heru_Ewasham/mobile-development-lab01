package yjhestem.ntnu.mobilewearableprogramming.lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class A2 extends AppCompatActivity {

    static final int FROM_NAME_REQURST = 1;  // The request code

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(A1.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.T2);
        textView.setText("Hello " + message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == A3.RESULT_OK){
                String result=data.getStringExtra("result");
                TextView textView = findViewById(R.id.T3);
                textView.setText("From A3: " + result);
            }
            if (resultCode == A3.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult

    public void setName(View view) {
        Intent fromNameIntent = new Intent(this, A3.class);
        //pickContactIntent.setType(Phone.CONTENT_TYPE); // Show user only contacts w/ phone numbers
        startActivityForResult(fromNameIntent, FROM_NAME_REQURST);
    }
}
