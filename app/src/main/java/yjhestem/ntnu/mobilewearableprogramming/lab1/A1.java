package yjhestem.ntnu.mobilewearableprogramming.lab1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class A1 extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "yjhestem.ntnu.mobilewearableprogramming.lab1.NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        // A slight modified code from: https://stackoverflow.com/questions/5241660/how-can-i-add-items-to-a-spinner-in-android
        String[] arraySpinner = new String[] {
                " Apple juice", "Orange juice", "Milk"
        };
        Spinner s = (Spinner) findViewById(R.id.L1);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        //adapter.setDropDownViewResource(android.R.layout.simple_spin‌​ner_dropdown_item);         // This is not necesarry (or possible to use as is).
        s.setAdapter(adapter);


        // to read the value from shared preferences.
        //
        // Have saved the value instead of position to handle cases where position of that object have changed.
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String l1Value = prefs.getString("favoriteDrinkDropdown", "");
        final String s1Value = prefs.getString("seekBarS1Progress", "0");

        SeekBar seekbarS1 = (SeekBar) findViewById(R.id.S1);
        seekbarS1.setProgress(Integer.parseInt(s1Value));
        s.setSelection(getIndex(s, l1Value));



        /**
         * When a value is changed, save value.
         *
         * I don't use onStopTrackingTouch and onStartTrackingTouch but it needs to be here.
         * Change because that would mean we had to save many unnecessary times.
         * @param seekBar
         */
        seekbarS1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch (SeekBar seekBar){

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                //int s1Value = seekBar.getProgress();
                setSharedVariable("seekBarS1Progress",String.valueOf(progress));
            }
        });

        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Lab1", "onItemSelected: Starting function");
                Spinner spinner=(Spinner) findViewById(R.id.L1);
                String drink = spinner.getSelectedItem().toString();

                // to store value in shared preferences
                setSharedVariable("favoriteDrinkDropdown",drink);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Another interface callback
            }
        });

    }

    /**
     * This method is used to find the strings position in the spinner/dropdown.
     *
     * Gotten from: https://stackoverflow.com/questions/8769368/how-to-set-position-in-spinner
     */
    private int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }

        return index;
    }


    /**
     * This function sets the shered preference "variables". This is used in two functions which now become one.
     * @param name
     * @param value
     */
    private void setSharedVariable(String name, String value){
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(name, value);
        editor.apply();
        Log.d("Lab1", "setSharedVariable: Set variable " + name + " to \"" + value + "\".");
    }




    /** Called when the user taps the Go button (function gotten from: https://developer.android.com/training/basics/firstapp/starting-activity.html#java) */
    public void gotoA2(View view) {
        Intent intent = new Intent(this, A2.class);
        EditText editText = (EditText) findViewById(R.id.T1);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

}
